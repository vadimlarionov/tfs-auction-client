package suits

import (
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/httpclient"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/requests"
)

type AuthTestSuite struct {
	TestSuite
}

func (s *AuthTestSuite) Run() {
	s.run(s.SignUpSignIn)
	s.run(s.SignUpDuplEmail)
	s.run(s.SignUpEmptyEmail)
	s.run(s.SignUpEmptyPassword)
	s.run(s.SignInUnknownUser)

	fmt.Printf("AuthTestSuite: passed=%d, failed=%d\n", s.passed, s.failed)
}

func NewAuthTestSuite(baseURL string, timeout time.Duration) *AuthTestSuite {
	return &AuthTestSuite{
		TestSuite: TestSuite{
			baseURL: baseURL,
			client:  httpclient.New(timeout),
		},
	}
}

func (s *AuthTestSuite) SignUpSignIn() error {
	email := uuid.New().String() + "@golang.org"
	const password = "test_password"
	if err := s.signUpSuccess(email, password, "1956-01-01"); err != nil {
		return fmt.Errorf("can't sign up:\n%s", err)
	}

	_, err := s.signInSuccess(email, password)
	if err != nil {
		return fmt.Errorf("can't sign in:\n%s", err)
	}

	return nil
}

func (s *AuthTestSuite) SignUpDuplEmail() error {
	email := uuid.New().String() + "@golang.org"
	const password = "test_password"
	if err := s.signUpSuccess(email, password, ""); err != nil {
		return fmt.Errorf("can't sign up:\n%s", err)
	}

	err := httpclient.Request{}.
		To(s.baseURL+signUpURL, http.MethodPost).
		WithRequest(requests.Register{
			FirstName: "Rob",
			LastName:  "Pike",
			Email:     email,
			Password:  "test_password",
		}).
		Execute(s.client).
		ExpectCode(http.StatusConflict).
		Validate()
	if err != nil {
		return err
	}
	return nil
}

func (s *AuthTestSuite) SignUpEmptyEmail() error {
	return httpclient.Request{}.
		To(s.baseURL+signUpURL, http.MethodPost).
		WithRequest(requests.Register{
			FirstName: "Rob",
			LastName:  "Pike",
			Email:     "",
			Password:  "test_password",
		}).
		Execute(s.client).
		ExpectCode(http.StatusBadRequest).
		Validate()
}

func (s *AuthTestSuite) SignUpEmptyPassword() error {
	email := uuid.New().String() + "@golang.org"
	return httpclient.Request{}.
		To(s.baseURL+signUpURL, http.MethodPost).
		WithRequest(requests.Register{
			FirstName: "Rob",
			LastName:  "Pike",
			Email:     email,
			Password:  "",
		}).
		Execute(s.client).
		ExpectCode(http.StatusBadRequest).
		Validate()
}

func (s *AuthTestSuite) SignInUnknownUser() error {
	email := uuid.New().String() + "@golang.org"
	return httpclient.Request{}.
		To(s.baseURL+signInURL, http.MethodPost).
		WithRequest(requests.Register{
			FirstName: "Rob",
			LastName:  "Pike",
			Email:     email, // unknown email
			Password:  "foo_bar",
		}).
		Execute(s.client).
		ExpectCode(http.StatusUnauthorized).
		Validate()
}
