package suits

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/httpclient"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/requests"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/responses"
	"gitlab.com/vadimlarionov/tfs-auction-client/internal/validator"
)

type UserTestSuite struct {
	TestSuite
}

func NewUserTestSuite(baseURL string, timeout time.Duration) *UserTestSuite {
	return &UserTestSuite{
		TestSuite: TestSuite{
			baseURL: baseURL,
			client:  httpclient.New(timeout),
		},
	}
}

func (s *UserTestSuite) loginByRandomUser() (*responses.User, string, error) {
	email := uuid.New().String() + "@golang.org"
	const password = "test_password"
	const birthday = "1956-01-01"
	if err := s.signUpSuccess(email, password, birthday); err != nil {
		return nil, "", fmt.Errorf("can't sign up: %s", err)
	}

	token, err := s.signInSuccess(email, password)
	if err != nil {
		return nil, "", fmt.Errorf("can't sign in: %s", err)
	}

	userResponse, err := s.getMe(token.AccessToken)
	if err != nil {
		return nil, "", fmt.Errorf("can't get me: %s", err)
	}

	return userResponse, token.AccessToken, nil
}

func (s *UserTestSuite) getMe(token string) (*responses.User, error) {
	resp := httpclient.Request{}.
		To(s.baseURL+"/users/0", http.MethodGet).
		WithToken(token).
		Execute(s.client).
		ExpectCode(http.StatusOK)
	if err := resp.Validate(); err != nil {
		return nil, fmt.Errorf("invalid response %+v: %s", resp, err)
	}

	var userResponse responses.User
	if err := json.Unmarshal([]byte(resp.Body), &userResponse); err != nil {
		return nil, fmt.Errorf("can't unmarshal response: %s", err)
	}

	return &userResponse, nil
}

func (s *UserTestSuite) Run() {
	s.run(s.UserGetMeWithBirthday)
	s.run(s.UpdateUser)
	fmt.Printf("UserTestSuite: passed=%d, failed=%d\n", s.passed, s.failed)
}

func (s *UserTestSuite) UserGetMeWithBirthday() error {
	email := uuid.New().String() + "@golang.org"
	const password = "test_password"
	const birthday = "1956-01-01"
	if err := s.signUpSuccess(email, password, birthday); err != nil {
		return fmt.Errorf("can't sign up: %s", err)
	}

	token, err := s.signInSuccess(email, password)
	if err != nil {
		return fmt.Errorf("can't sign in: %s", err)
	}

	userResponse, err := s.getMe(token.AccessToken)
	if err != nil {
		return fmt.Errorf("can't get me: %s", err)
	}

	v := validator.Validator{}.
		Int64Gt0("id", userResponse.ID).
		EqualString("email", email, userResponse.Email).
		EmptyString("password", userResponse.Password).
		EqualString("first_name", "Rob", userResponse.FirstName). // TODO
		EqualString("last_name", "Pike", userResponse.LastName).
		NotEmptyTime("created_at", userResponse.CreatedAt).
		//NotEmptyTime("updated_at", userResponse.UpdatedAt).
		EqualString("birthday", userResponse.BirthDay, birthday)

	if err := v.Validate(); err != nil {
		return fmt.Errorf("invalid response:\n%s", err)
	}

	return nil
}

func (s *UserTestSuite) UpdateUser() error {
	user, token, err := s.loginByRandomUser()
	if err != nil {
		return fmt.Errorf("can't login by random user: %s", err)
	}

	req := requests.UserToUpdate{
		FirstName: "Dave",
		LastName:  "Cheney",
		Birthday:  "",
	}

	resp := httpclient.Request{}.
		To(s.baseURL+"/users/0", http.MethodPut).
		WithToken(token).
		WithRequest(req).
		Execute(s.client).
		ExpectCode(http.StatusOK)
	if err := resp.Validate(); err != nil {
		return fmt.Errorf("invalid response %+v: %s", resp, err)
	}

	var updatedUser responses.User
	if err := json.Unmarshal([]byte(resp.Body), &updatedUser); err != nil {
		return fmt.Errorf("can't unmarshal updated user: %s", err)
	}

	v := validator.Validator{}.
		EqualString("first_name", "Dave", updatedUser.FirstName).
		EqualString("last_name", "Cheney", updatedUser.LastName).
		EqualString("email", user.Email, updatedUser.Email).
		EqualString("birthday", "", updatedUser.BirthDay)
	if err := v.Validate(); err != nil {
		return fmt.Errorf("invalid response:\n%s", err)
	}

	return nil
}
