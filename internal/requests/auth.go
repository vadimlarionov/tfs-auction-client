package requests

type Register struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Birthday  string `json:"birthday,omitempty"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

type Authorize struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserToUpdate struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Birthday  string `json:"birthday,omitempty"`
}
