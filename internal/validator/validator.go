package validator

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

type Validator struct {
	errors []string
}

func (v Validator) EqualString(name string, expected, actual string) Validator {
	if expected != actual {
		msg := fmt.Sprintf("invalid string field %q: expected=%+v, actual=%+v", name, expected, actual)
		v.errors = append(v.errors, msg)
	}
	return v
}

func (v Validator) Int64Gt0(name string, actual int64) Validator {
	if actual <= 0 {
		msg := fmt.Sprintf("field %q must be great than 0: actual=%d", name, actual)
		v.errors = append(v.errors, msg)
	}
	return v
}

func (v Validator) EmptyString(name string, actual string) Validator {
	if actual != "" {
		msg := fmt.Sprintf("field %q must be empty: actual=%+v", name, actual)
		v.errors = append(v.errors, msg)
	}
	return v
}

// nolint: gocyclo
func (v Validator) EqualDate(name string, expected, actual *time.Time) Validator {
	var msg string
	switch {
	case expected == nil && actual == nil:
	case expected != nil && actual != nil:
		if !expected.Equal(*actual) {
			msg = fmt.Sprintf("date field %q must equal: expected=%s, actual=%s", name, *expected, *actual)
		}
	case expected == nil && actual != nil:
		msg = fmt.Sprintf("field %q must be nil: actual=%s", name, *actual)
	case expected != nil && actual == nil:
		msg = fmt.Sprintf("field %q must not be nil: expected=%s", name, *expected)
	}

	if msg != "" {
		v.errors = append(v.errors, msg)
	}

	return v
}

func (v Validator) NotEmptyTime(name string, actual time.Time) Validator {
	if actual.IsZero() {
		msg := fmt.Sprintf("field %q is zero", name)
		v.errors = append(v.errors, msg)
	}
	return v
}

func (v Validator) Validate() error {
	if len(v.errors) != 0 {
		return errors.New(strings.Join(v.errors, "\n"))
	}

	return nil
}
