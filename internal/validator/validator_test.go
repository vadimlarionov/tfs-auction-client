package validator

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestValidator_EqualString(t *testing.T) {
	r := require.New(t)
	r.NoError(Validator{}.EqualString("foo", "foo", "foo").Validate())
	r.Error(Validator{}.EqualString("bar", "bar1", "bar2").Validate())
}

func TestValidator_EmptyString(t *testing.T) {
	r := require.New(t)
	r.NoError(Validator{}.EmptyString("foo", "").Validate())
	r.Error(Validator{}.EmptyString("bar", "bar1").Validate())
}

func TestValidator_EqualDate(t *testing.T) {
	r := require.New(t)
	t1 := time.Date(2019, 03, 31, 22, 14, 0, 0, time.Local)
	t2 := time.Date(2019, 02, 31, 22, 14, 0, 0, time.Local)
	r.NoError(Validator{}.EqualDate("d1", &t1, &t1).Validate())
	r.Error(Validator{}.EqualDate("d1", nil, &t1).Validate())
	r.Error(Validator{}.EqualDate("d1", &t1, nil).Validate())
	r.Error(Validator{}.EqualDate("d1", &t1, &t2).Validate())
}
