package responses

import "time"

type Token struct {
	TokenType   string `json:"token_type"`
	AccessToken string `json:"access_token"`
}

func (t Token) IsBearer() bool {
	return t.TokenType == "bearer"
}

type User struct {
	ID        int64     `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	BirthDay  string    `json:"birth_day"`
	Email     string    `json:"email"`
	Password  string    `json:"password"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
