package httpclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

const jsonContentType = "application/json"

type Suite struct {
	Request  Request
	Response Response
}

type Request struct {
	targetURL  string
	httpMethod string
	req        interface{}
	header     http.Header
}

func (tc Request) To(target, httpMethod string) Request {
	tc.targetURL = target
	tc.httpMethod = httpMethod
	return tc
}

func (tc Request) WithRequest(req interface{}) Request {
	tc.req = req
	return tc
}

func (tc Request) WithHeader(key, value string) Request {
	if tc.header == nil {
		tc.header = make(http.Header)
	}

	tc.header.Add(key, value)
	return tc
}

func (tc Request) WithToken(token string) Request {
	if tc.header == nil {
		tc.header = make(http.Header)
	}

	tc.header.Set("Authorization", "Bearer "+token)
	return tc
}

func (tc Request) Execute(c *Client) Response {
	resp, err := c.Do(tc)
	if err != nil {
		return Response{err: err}
	}

	return *resp
}

type Response struct {
	code         int
	expectedCode int
	Body         string
	header       http.Header

	err error
}

func (r Response) ExpectCode(expected int) Response {
	r.expectedCode = expected
	return r
}

func (r Response) Validate() error {
	if r.err != nil {
		return fmt.Errorf("response has error: %s", r.err)
	}

	var errMsg string
	if r.expectedCode > 0 {
		if r.expectedCode != r.code {
			errMsg += fmt.Sprintf("\twrong http code: expected=%d, actual=%d\n", r.expectedCode, r.code)
		}
	}

	if errMsg != "" {
		return fmt.Errorf("%s", errMsg)
	}

	return nil
}

type Client struct {
	httpClient http.Client
}

func New(timeout time.Duration) *Client {
	return &Client{
		httpClient: http.Client{Timeout: timeout},
	}
}

func (c *Client) Do(tcReq Request) (*Response, error) {
	var reqReader io.Reader
	if tcReq.req != nil {
		b, err := json.Marshal(tcReq.req)
		if err != nil {
			return nil, errors.Wrapf(err, "can't marshal request body")
		}

		reqReader = bytes.NewReader(b)
	}

	req, err := http.NewRequest(tcReq.httpMethod, tcReq.targetURL, reqReader)
	if err != nil {
		return nil, errors.Wrapf(err, "can't create request to %q", tcReq.targetURL)
	}
	req.Header.Set("Content-Type", jsonContentType)

	for key, values := range tcReq.header {
		for _, v := range values {
			req.Header.Add(key, v)
		}
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "can't execute request")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "can't read response body")
	}
	defer resp.Body.Close()

	tcResp := Response{
		code:   resp.StatusCode,
		Body:   string(body),
		header: resp.Header,
	}

	return &tcResp, nil
}
