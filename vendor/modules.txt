# github.com/davecgh/go-spew v1.1.1
github.com/davecgh/go-spew/spew
# github.com/google/uuid v1.1.1
github.com/google/uuid
# github.com/pkg/errors v0.8.1
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
github.com/pmezard/go-difflib/difflib
# github.com/stretchr/testify v1.3.0
github.com/stretchr/testify/require
github.com/stretchr/testify/assert
# go.uber.org/atomic v1.3.2
go.uber.org/atomic
# go.uber.org/multierr v1.1.0
go.uber.org/multierr
# go.uber.org/zap v1.9.1
go.uber.org/zap
go.uber.org/zap/zapcore
go.uber.org/zap/internal/bufferpool
go.uber.org/zap/buffer
go.uber.org/zap/internal/color
go.uber.org/zap/internal/exit
