package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/vadimlarionov/tfs-auction-client/internal/suits"
)

type config struct {
	Target      string
	HTTPTimeout time.Duration
}

func parseConfig() config {
	cfg := config{}

	flag.StringVar(&cfg.Target, "target", "http://localhost:5000", "Target host.")
	flag.DurationVar(&cfg.HTTPTimeout, "http-timeout", 5*time.Second, "HTTP timeout for request to target.")

	flag.Parse()

	return cfg
}

func main() {
	cfg := parseConfig()
	fmt.Printf("TFS-Autction-Client started with config %+v\n", cfg)

	authSuite := suits.NewAuthTestSuite(cfg.Target, cfg.HTTPTimeout)
	authSuite.Run()

	//userSuite := suits.NewUserTestSuite(cfg.Target, cfg.HTTPTimeout)
	//userSuite.Run()
}
