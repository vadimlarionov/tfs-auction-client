# TFS Auction Client
HTTP-клиент для проекта «‎Аукцион»

[Swagger-документация](http://51.15.55.222)

## Запуск
```sh
go run cmd/tfs-auction-client/main.go --target="http://localhost:5000"
```

## Сборка и запуск
```sh
go build -o $(go env GOPATH)/bin/tfs-auction-client cmd/tfs-auction-client/main.go
tfs-auction-client --target="http://localhost:5000"
```

Либо через команду make
```sh
make build
tfs-auction-client --target="http://localhost:5000"
```